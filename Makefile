minikube-all: minikube-start sealedsecrets-own-cert argocd-install sealedsecrets-install aiu-install minikube-app-mybirds
# minikube-all: minikube-start sealedsecrets-own-cert argocd-install minikube-init minikube-aui-ss minikube-app-mybirds
gke-all: gke-install sealedsecrets-own-cert argocd-install gke-init gke-aui-ss gke-app-mybirds
# gke-all: gke-install sealedsecrets-own-cert argocd-install argocd-post-step gke-install gke-init gke-aui-ss TODO gke-app-mybirds

# common
argocd-install:
	kubectl create ns argocd
	helm repo add argo https://argoproj.github.io/argo-helm
	# TODO install with own chart values
	helm upgrade --install argocd argo/argo-cd --version 3.26.11 --namespace argocd
argocd-post-step:
	# TODO is it needed?
	# echo "sleep for 60 sec to make sure that argocd server has been started"
	# sleep 60
	kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo "\n"
	kubectl -n argocd port-forward svc/argocd-server 8080:80 &
sealedsecrets-own-cert:
	kubectl -n "kube-system" create secret tls "mycustomkeys" --cert="/Users/a.horbach/.config/sealedsecrets/ss_tls.crt" --key="/Users/a.horbach/.config/sealedsecrets/ss_tls.key"

# GKE
gke-install:
	# gcloud container clusters create test --num-nodes=3 --zone=europe-central2-a
	gcloud container clusters create demo --num-nodes=3 --zone=europe-north1-a
gke-init:
	kubectl create -f ./90-gke/01-init.yml
	echo "Waiting for L4LB exeternal ip"
	# TODO - how much we need to wait? while loop?
	sleep 60
	kubectl -n default get service ingress-nginx-controller  -o json | jq -r '.status.loadBalancer.ingress[].ip'
	# TODO - add NLB ip to mb chart values
	ip=$(kubectl -n default get service ingress-nginx-controller  -ojson | jq -r '.status.loadBalancer.ingress[].ip')
	sed -i "s|3[0-9]\.[0-9]\{3\}\.[0-9]\{3\}\.[0-9]\{3\}|${ip}|" ./90-gke/03-app-mybirds/application-mybirds.yml
	git pull --rebase
	git add ./90-gke/03-app-mybirds/application-mybirds.yml
	git commit -m "chore(makefile): auto commit makefile [ci skip]"
	git push
	echo "<gke-init> step has been done"
gke-aui-ss:
	kubectl create -f ./90-gke/02-aiu-ss.yml
	echo "<gke-aui-ss> step has been done"
gke-app-mybirds:
	kubectl create -f ./90-gke/03-app-mybirds.yml
	sleep 75
	echo "waiting for postgress"
	kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
	sleep 45
	kubectl -n mybirds-ns rollout restart deploy mybirds
	kubectl -n mybirds-ns get ing

# minikube:
minikube-start:
	minikube start
	minikube addons enable ingress
	minikube addons enable ingress-dns
	sudo sed -e "s|192.168.64.[0-9]\{2,3\}|$(minikube ip)|" /etc/resolver/minikube-minikube-dev

sealedsecrets-install:
	helm repo add bitnami-labs https://bitnami-labs.github.io/sealed-secrets/
	helm upgrade --install sealed-secrets bitnami-labs/sealed-secrets --version 1.16.1 --namespace kube-system --set secretName=mycustomkeys
	kubectl create -f ./10-minik8s/02-aiu-ss

aiu-install:
	helm repo add argo https://argoproj.github.io/argo-helm
	helm upgrade --install argocd-image-updater argo/argocd-image-updater --version 0.5.0 --namespace argocd -f ./10-minik8s/aui.values.yaml

minikube-app-mybirds:
	kubectl create -f ./10-minik8s/03-app-mybirds.yml
	sleep 60
	echo "waiting for postgress"
	kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
	sleep 45
	kubectl -n mybirds-ns rollout restart deploy mybirds

## GKE way
# minikube-init:
# 	kubectl create -f ./10-minik8s/01-init.yml
# 	echo "<minik8s-init> step has been done"
# minikube-aui-ss:
# 	kubectl create -f ./10-minik8s/02-aiu-ss.yml
# 	echo "<gke-aui-ss> step has been done"
# minikube-app-mybirds:
# 	kubectl create -f ./10-minik8s/03-app-mybirds.yml
# 	sleep 30
# 	echo "waiting for postgress"
# 	kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
# 	kubectl -n mybirds-ns get ing

# start-minikube:
# 	minikube start
# 	minikube addons enable ingress
# 	minikube addons enable ingress-dns
# 	# TODO
# 	# sudo sed -i "s|ipaddr|$(minikube ip)" /etc/resolver/minikube-minikube-dev  
# aiu-install:
# 	helm repo add argo https://argoproj.github.io/argo-helm
# 	helm upgrade --install argocd-image-updater argo/argocd-image-updater --version 0.5.0 --namespace argocd

# sealedsecrets-install:
# 	helm repo add bitnami-labs https://bitnami-labs.github.io/sealed-secrets/
# 	helm upgrade --install sealed-secrets bitnami-labs/sealed-secrets --version 1.16.1 --namespace kube-system
# sealedsecrets-own-cert:
# 	kubectl -n "kube-system" create secret tls "mycustomkeys" --cert="/Users/a.horbach/.config/sealedsecrets/ss_tls.crt" --key="/Users/a.horbach/.config/sealedsecrets/ss_tls.key"
# 	# kubectl -n "kube-system" label secret "mycustomkeys" sealedsecrets.bitnami.com/sealed-secrets-key=active
# 	# kubectl -n "kube-system" rollout restart deploy sealed-secrets
# ing-controller-install:
# 	helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
# 	helm upgrade --install nginx-ingress ingress-nginx/ingress-nginx --version 4.0.10 -n default
# 	sleep 15
# 	kubectl get service nginx-ingress-ingress-nginx-controller -ojson | jq -r '.status.loadBalancer.ingress[].ip'
# mybirds-mini-install:
# 	kubectl create -f ./10-minik8s/_init/application-mybirds.yml
# 	echo "wait 10 sec, than run migrations"
# 	sleep 10
# 	kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
# 	echo "wait 20 sec, than restart mybirds deployments"
# 	sleep 20
# 	kubectl -n mybirds-ns rollout restart deploy mybirds
# 	echo "wait 20 sec, all should fly now"
# mybirds-check-minikube:
# 	nslookup mybirds.dev $(minikube ip)
# 	http GET http://mybirds.dev/ht/ --check-status --json
# gke-install:
# 	gcloud container clusters create test --num-nodes=3 --zone=europe-central2-a
# gke-init:
# 	kubectl create -f ./apps/gke.yml
# gke-lb-ip:
# 	echo "Waiting for l4lb exeternal ip"
# 	sleep 60
# 	kubectl get service nginx-ingress-ingress-nginx-controller -ojson | jq -r '.status.loadBalancer.ingress[].ip'


# gke-install:
# 	gcloud container clusters create test --num-nodes=3 --zone=europe-central2-a
# sealedsecrets-own-cert:
# 	kubectl -n "kube-system" create secret tls "mycustomkeys" --cert="/Users/a.horbach/.config/sealedsecrets/ss_tls.crt" --key="/Users/a.horbach/.config/sealedsecrets/ss_tls.key"
# argocd-install:
# 	kubectl create ns argocd
# 	helm repo add argo https://argoproj.github.io/argo-helm
# 	helm upgrade --install argocd argo/argo-cd --version 3.26.11 --namespace argocd
# gke-init:
# 	kubectl create -f ./gke/01-init.yml
# 	echo "Waiting for l4lb exeternal ip"
# 	sleep 60
# 	ip=$(kubectl -n default get service ingress-nginx-controller -ojson | jq -r '.status.loadBalancer.ingress[].ip')
# 	# yq/sed -i "ip: r'ip'|${ip}"./90-gke/03-app-mybirds/application-mybirds.yml
# 	kubectl create -f ./gke/02-aiu-ss.yml
# 	kubectl create -f ./gke/03-app-mybirds.yml
# 	sleep 30
# 	kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
