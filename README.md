# Overview

Приложение доступно по адресу: https://34.118.59.86.nip.io/ (GKE кластер, доступ через L4LB и nginx ingress controller, сертификат не валидный).

Закрыто basic auth, за креденшиалами к andrewhorbach@gmail.com

# K8s env

Окружение для запуска приложения.

Проверено на BigSur 11.5.2

# Развертывание

## Запуск minikube

```bash
minikube start --cpus=2 --memory=4gb --disk-size=25gb --vm-driver=hyperkit
minikube addons enable ingress
minikube addons enable ingress-dns
```

## Установка Argo CD

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```
Через некоторое время все служебные поды argo поднимутся, и он сможет подтягивать состояние кластера из git-а.

## Доступ к UI ARGO

```bash
# Получаем пароль от UI
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d 
# Делаем port-forwarding для UI
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

Заходим в веб интерфейс, логин админ, пароль получили в предыдущей команде.

## DNS конфиг:

Документация по ing-dns [тут](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/).

Добавляем в директорию `/etc/resolver` файл с именем minikube-<profile>-<домен>.

Профайл можно получить с помощью `$ minikube profile list`, в моем случае это `minikube`; домен - `test`.

Таким образом имя файла `minikube-minikube-test`, содержимое ->:

```
domain test
nameserver 192.168.64.39
search_order 1
timeout 5
```

Адрес кластера миникуб можно получить с помощью `minikube ip`.

## Создание CRD argo app

```bash
kubectl create -f services-dev.yml
```

## Применяем миграции.

Т.к. у нас gitops, то джобу миграций удобно породить из кронджобы.

!NOTE: чтобы джоба сработала, нужно чтобы постгрес был готов принимать соединения.
```bash
kubectl create job -n dev --from=cronjob/create-django-su migrations
```

### GKE

```bash
# Стартуем кластер
gcloud container clusters create my-birds --num-nodes=2 --zone=europe-central2-a
# Забираем конфиг
gcloud container clusters get-credentials my-birds --zone=europe-central2-a
# Удаляем
gcloud container clusters delete my-birds --zone=europe-central2-a
```

**ing**
https://cloud.google.com/community/tutorials/nginx-ingress-gke

## Установка SealedSecrets
```shell
export PRIVATEKEY="ss_tls.key"
export PUBLICKEY="ss_tls.crt"
export NAMESPACE="kube-system"
export SECRETNAME="mycustomkeys"
```

```shell
kubectl -n "$NAMESPACE" create secret tls "$SECRETNAME" --cert="$PUBLICKEY" --key="$PRIVATEKEY"
kubectl -n "$NAMESPACE" label secret "$SECRETNAME" sealedsecrets.bitnami.com/sealed-secrets-key=active

kubectl rollout restart deploy sealed-secrets -n kube-system
```

# GNU make
## Устанавливаем minikube

> В make файле кластер minikube запускается без каких-то параметров, рекомендуется их выставить с помощью `minikube config`
<details>
<summary>minikube config</summary>

Посмотреть список параметров:
```shell
❯ minikube config
config modifies minikube config files using subcommands like "minikube config set driver kvm2"
Configurable fields:

 * driver
 * vm-driver
 * container-runtime
...

Available Commands:
  defaults    Lists all valid default values for PROPERTY_NAME
  get         Gets the value of PROPERTY_NAME from the minikube config file
  set         Sets an individual value in a minikube config file
  unset       unsets an individual value in a minikube config file
  view        Display values currently set in the minikube config file

Usage:
  minikube config SUBCOMMAND [flags] [options]

Use "minikube <command> --help" for more information about a given command.
Use "minikube options" for a list of global command-line options (applies to all commands).
```

Установить необходимы параметры для osX, например размер диска виртуальной машины однонодного кластера:
```shell
minikube config set disk-size 20gb
```

</details>

---
Стартуем кластер, устанавливаем аддоны - ingress и ingress-dns
```shell
make start-minikube
```
Устанавливаем sealedsecrets оператор и подсовываем ему наш tls сертификат, чтобы расшифровать ранее зашифрованные секреты (подробнее [тут](https://github.com/bitnami-labs/sealed-secrets/blob/main/docs/bring-your-own-certificates.md)):
> Естественно вам нужно сгенерировать свой сертификат, и пути к сертификату в make файле мои, нужно допиливать этот пункт.
```shell
make sealedsecrets-install
```
Устанавливаем helm - чарт:
```shell
helm upgrade --install mb ./.helm/generic-web-app -f ./10-minik8s/application-mybirds/values.yaml --debug
```
Запускаем Job для выполнения миграций схемы бд и etc:
<!---
# TODO - make it via helm post hooks
-->
```shell
# kubectl -n <ns> create job --from=cronjob/<cj-name> <job-name>
kubectl -n mybirds-ns create job --from=cronjob/mybirds-cj project-init
```
Так же нужно добавить адрес кластера minik8s в локальный dns резовер:

**DNS конфиг**

Документация по ing-dns [тут](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/).

Добавляем в директорию `/etc/resolver` файл с именем minikube-<profile>-<домен>.

Профайл можно получить с помощью `$ minikube profile list`, в моем случае это `minikube`; домен - `test`.

Таким образом имя файла `minikube-minikube-test`, содержимое ->:

```
domain test
nameserver 192.168.64.39
search_order 1
timeout 5
```
