{{/* vim: set filetype=mustache: */}}
{{- define "app.name" -}} {{/* Fetch app name from values.yml or set default name as name of the chart dir */}}
{{- $n := .Values.appConfig.name -}}
  {{- if $n }}
  {{- default $n -}}
  {{- else -}}
  {{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
  {{- end -}}
{{- end -}}

{{- define "app.ns" -}} {{/* <app.name> + suffix for ns */}}
{{- default (printf "%s-ns" (include "app.name" .)) -}}
{{- end -}}

{{- define "db.name" -}} {{/* <mb.name> + db suffix */}}
{{- default (printf "%s-postgresql" (include "app.name" .)) -}}
{{- end -}}

{{- define "db.volume" -}} {{/* <db.name> + volume suffix */}}
{{- $volume := printf "%s-volume" (include "db.name" .) -}}
{{- default $volume -}}
{{- end -}}

{{- define "sh" -}} {{/* entrypoint sh -c helper */}}
{{- $entrypoint := list "sh" "-c" -}}
{{- default $entrypoint -}}
{{- end -}}

{{- define "envs" -}} 
{{/* 
Iterator for specify all regular envs for a container and envs from secret as well.
You need to keep fallowing structure in your values.yaml:
env:  # <- map{} for regular env vars
  ENV_VAR: value
  foo: bar
envSecrets:  # <- map{} for envs from secrets
  SECRET_VAR: k8s-secret-name
---
! NOTE: env var from secret in a container will have the same key as in k8s object secret.data. 
*/}}
{{- range $key, $value := .Values.appConfig.env }}
- name: {{ $key }}
  value: {{ $value | quote }}
{{- end }}
{{- range $key, $secret := .Values.appConfig.envSecrets }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      name: {{ $secret }}
      key: {{ $key | quote }}
{{- end }}
{{- end -}}
